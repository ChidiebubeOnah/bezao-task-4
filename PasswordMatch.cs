﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using BEZA0___Task_4.Models;

namespace BEZA0___Task_4
{
    public class PasswordMatch: ValidationAttribute
    {
        protected override ValidationResult IsValid(object value,
            ValidationContext validationContext)
        {


            var user = (User) validationContext.ObjectInstance;
            if (user.Password1 == null)
            {
                return new ValidationResult("Enter Password");
            }

            if (value == null)
            {
                return new ValidationResult("Confirm Password");
            }

            return user.Password1 != null && (user.Password1 == (string) value)
                ? ValidationResult.Success
                : new ValidationResult("Password does not match");
        }
    }

    public class Min18IfAMember : ValidationAttribute
    {
       
    }
}