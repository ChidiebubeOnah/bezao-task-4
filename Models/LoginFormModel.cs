﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BEZA0___Task_4.Models
{
    public class LoginFormModel
    {
        [Required]
        public string UserName { get; set; }
        
        [Required]
        public string Password { get; set; }


        public bool IsSelected { get; set; }
        public string Message { get; set; }
    }
}