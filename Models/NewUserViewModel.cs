﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BEZA0___Task_4.Models
{
    public class NewUserViewModel
    {
        public User User { get; set; }
        public bool IsSelected { get; set; }

        public string Message { get; set; }
    }
}