﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Security.Cryptography;

namespace BEZA0___Task_4.Helpers
{
    public class UserRepository
    {



        private static readonly string Constr = ConfigurationManager.ConnectionStrings["Task4"].ConnectionString;

        public static void CreateUser(string fullname, string username, string email, string password)
        {

            using (SqlConnection con = new SqlConnection(Constr))
            {
                string query =
                    "INSERT INTO Users(FullName, UserName, Email, Password) VALUES(@FullName, @UserName, @Email, @Password)";

                using (SqlCommand cmd = new SqlCommand(query))
                {

                    cmd.Parameters.AddWithValue("@FullName", fullname);
                    cmd.Parameters.AddWithValue("@UserName", username);
                    cmd.Parameters.AddWithValue("@Email", email);
                    cmd.Parameters.AddWithValue("@Password", HashAndSalt.PasswordHash(password));
                    cmd.Connection = con;
                    con.Open();
                    try
                    {
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        throw e;
                    }

                    con.Close();
                }

            }
        }

        public static (byte[] hashBytes, byte[] hash, string username ) ComparePassword(string username, string password)
        {


        byte[] hashBytes = { };

        byte[] hash = { };

        string Username = "";
        using (SqlConnection con = new SqlConnection(Constr))
        {
            string query = "SELECT * FROM Users WHERE UserName=@Username";

            using (SqlCommand cmd = new SqlCommand(query))
            {

                cmd.Parameters.AddWithValue("@Username", username);

                cmd.Connection = con;
                con.Open();
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            string savedPasswordHash = reader["Password"].ToString();
                            username = reader["UserName"].ToString();
                            hashBytes = Convert.FromBase64String(savedPasswordHash);
                            byte[] salt = new byte[16];
                            Array.Copy(hashBytes, 0, salt, 0, 16);
                            var pbkdf2 = new Rfc2898DeriveBytes(password, salt, 10000);
                            hash = pbkdf2.GetBytes(20);
                        }

                    }
                    else
                    {
                        throw  new IndexOutOfRangeException("Invalid Credentials");
                    }
                }

            }

            con.Close();
        }

        return (hashBytes, hash, username);
        }
        } 

}