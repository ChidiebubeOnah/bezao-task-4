﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.Mvc;
using BEZA0___Task_4.Helpers;
using BEZA0___Task_4.Models;

namespace BEZA0___Task_4.Controllers
{
    public class UsersController : Controller
    {
        // GET: Users
       [Route("")]
        public ActionResult Index()
        {
            if (Session["UserName"] != null)
                return View();
            return RedirectToAction("Login");
        }

        public ActionResult Register()
        {
            var viewModel = new NewUserViewModel
            {
                IsSelected = true
            };

            return View(viewModel);
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(User user)
        {


            if (!ModelState.IsValid)
            {
                var viewModel = new NewUserViewModel
                {
                    User = user,
                    IsSelected = true
                };

                return View("Register", viewModel);
            }

            try
            {
                UserRepository.CreateUser(user.FullName, user.UserName, user.Email, user.Password2);
            }
            catch (SqlException )
            {
                var viewModel = new NewUserViewModel
                {
                    User = user,
                    IsSelected = true,
                    Message = "The Email or Username Already Exist in the Database!"
                };
                return View("Register", viewModel);

            }
           
            return RedirectToAction("Login");

        }

        public ActionResult Login()
        {

            var viewModel = new LoginFormModel
            {
                IsSelected = true,
            };
            return View(viewModel);
        }

        public ActionResult Authenticate(LoginFormModel model)
        {
            if (!ModelState.IsValid)
            {
                model.IsSelected = true;

                return View("Login", model);
            }

            try
            {
                var compare = UserRepository.ComparePassword(model.UserName, model.Password);
                int ok = 1;
                for (int i = 0; i < 20; i++)
                {
                    if (compare.hashBytes[i + 16] != compare.hash[i])
                        ok = 0;

                    if (ok == 1)
                    {
                        Session["UserName"] = compare.username;
                        return RedirectToAction("Index", "Users");

                    }

                }
            }
            catch (IndexOutOfRangeException)
            {

                model.Message = "Invalid Credentials";
               
            }
            model.Message = "Invalid Credentials";
            return View("Login", model);
        }

        
        public ActionResult Logout()
        {
            Session.Remove("UserName");

            return RedirectToAction("Login");
        }
    }
}
